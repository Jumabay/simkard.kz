const form = document.querySelector("form");

(() => {
  const formTel = document.querySelector("input[type='tel']");
  new IMask(formTel, {
    mask: "+{7} (000) 000-00-00",
  });
})();

form.addEventListener("submit", function (event) {
  event.preventDefault();

  api("telegram/telegram.php", {
    method: "POST",
    body: new FormData(form),
  }).then(({ json, response }) => {
    

    const data = JSON.parse(response);

    // успешно
    if (data.success === true) {
      // кинуть уведомление
      alert("Ваш запрос прошёл успешно");

      // перевести на другую страницу
      // location.href = '/thank-you.html';
    }

    // успешно
    if (data.error === true) {
      // кинуть уведомление
      alert(data.message);
    }
  });
});
//
