from django.urls import path

from . import views

urlpatterns = [
    path('login/', views.loginPage, name="login"),
    path('registration/', views.registrationPage, name="registration"),

    path('', views.home, name="home"),
    path('detail/<int:id>/', views.detail, name="detail"),
    path('my/', views.my, name="my"),
    path('add/', views.add, name="add"),
    path('edit/<int:id>/', views.edit, name="edit"),
    path('delete/<int:id>/', views.delete, name="delete"),
    # Страница продавца
    path('seller/<int:id>/', views.profileSeller, name='seller'),
    # Ссылка, куда отправляются пост запросы з поиском номера телефона
    path('find_numbers/', views.find_number, name='find_number')
]
