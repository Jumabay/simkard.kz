from django.db import models
from django.conf import settings
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from phone_field import PhoneField



class PhoneNumber(models.Model):
	operators = [
		(1, 'activ'),
		(2, 'kcell'),
		(3, 'altel'),
		(4, 'tele2'),
		(5, 'beeline'),
		(6, 'izi')
		]
	
	operator = models.PositiveSmallIntegerField(("Оператор"), choices=operators)
	msisdn = PhoneField(blank=True, help_text='Номер телефона', max_length=10)
	price = models.IntegerField('Цена')
	cities = [
		(1, 'Алматы'),
		(2, 'Нур-Султан'),
		(3, 'Шымкент'),
		(4, 'Караганда'),
		(5, 'Павлодар'),
		(6, 'Кызылорда'),
		(7, 'Семей'),
		(8, 'Атырау'),
		(9, 'Актобе'),
		(10, 'Актау'),
		(11, 'Уральск'),
		(12, 'Усть-Каменогорск'),
		(13, 'Кокшетау'),
		(14, 'Петропавловск'),
		(15, 'Талдыкорган'),
		(16, 'Жезказган'),

	]

	is_rent = models.BooleanField(('Аренда'), default=False)

	city = models.PositiveSmallIntegerField(("Город"), choices=cities)
	description = models.CharField('Описание', max_length=200)
	contact_phone = PhoneField(blank=True, help_text='Контактный телефон', max_length=10)
	owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='seller', on_delete=models.CASCADE,)
	created = models.DateTimeField(auto_now_add=True)
	active = models.BooleanField(default=False)
	is_top = models.BooleanField(('Топ номер'), default=False)

	def __str__(self):
		return str(self.msisdn)

	class Meta:
		verbose_name = 'Красивый номер'
		verbose_name_plural = 'Красивые номера' 

	@property
	def css_class(self):
		s = self.get_operator_display()
		return "title__text_%s" % s

	@property
	def price_html(self):
		if self.is_rent:
			return "%d тг/меc" % self.price
		return "%d тг" % self.price

	# Получение ссылки на страницу номера
	def get_absolute_url(self):
		return reverse('detail', args=[self.pk])

